import org.w3c.dom.ls.LSOutput;

import java.time.LocalDate;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class AutorizacaoCirurgia {
    
    //atributos e construtor
    private int codAut;
    private Date dataCadastro;
    private Medico medicoSoli;
    private Paciente paciente;
    private Cirurgia cirurgiaSoli;

    public AutorizacaoCirurgia(Date dataCadastro, Medico medicoSoli,Paciente paciente,Cirurgia cirurgiaSoli){
        this.codAut = (int) System.currentTimeMillis();
        this.dataCadastro = dataCadastro;
        this.medicoSoli = medicoSoli;
        this.paciente = paciente;
        this.cirurgiaSoli = cirurgiaSoli;
    }


    //metodos especiais
    public int getCodAut() {
        return codAut;
    }

    public void setCodAut(int codAut) {
        this.codAut = codAut;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Medico getMedicoSoli() {
        return medicoSoli;
    }

    public void setMedicoSoli(Medico medicoSoli) {
        this.medicoSoli = medicoSoli;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Cirurgia getCirurgiaSoli() {
        return cirurgiaSoli;
    }

    public void setCirurgiaSoli(Cirurgia cirurgiaSoli) {
        this.cirurgiaSoli = cirurgiaSoli;
    }


    @Override
    public String toString() {
        return "{" +
            " codAut='" + getCodAut() + "'" +
            ", dataCadastro='" + getDataCadastro() + "'" +
            ", medicoSoli='" + getMedicoSoli() + "'" +
            ", paciente='" + getPaciente() + "'" +
            ", cirurgiaSoli'" + getCirurgiaSoli() + "'" +
            "}";
    }
    

    

    




}
