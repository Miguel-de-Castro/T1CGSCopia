public abstract class Usuario {

    //atributos
    private int codUsuario;
    private String nome;
    private String iniciais;

    public Usuario(int codUsuario, String nome, String iniciais){
        this.codUsuario = codUsuario;
        this.nome = nome;
        this.iniciais = iniciais;
    }

    //metodos especiais
    public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario){
        this.codUsuario = codUsuario;
    }

    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getIniciais() {
        return iniciais;
    }
    
}