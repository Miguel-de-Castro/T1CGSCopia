import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Exame {
    private TipoExames tipo;
    private boolean isRealizado;
    public int numExame = 0;

    public int getNumExame() { return numExame; }

    Exame(TipoExames tipo){
        this.tipo = tipo;
        isRealizado = false;
        numExame++;
    }

    public TipoExames getTipo() {
        return tipo;
    }

    //verifica se as datas estão no intevalo de 30 dias
    public boolean setRelizado(Date dataCadastro, Date dataRealizado){
        double diferencaTrinta = 2592000000.0;
        if(dataCadastro.before(dataCadastro)) return false;
        else if((dataRealizado.getTime() - dataCadastro.getTime()) < diferencaTrinta) {
            return isRealizado = true;
        }
        return isRealizado = true;
    }

    public boolean isRelizado(){
        return isRealizado;
    }


}
