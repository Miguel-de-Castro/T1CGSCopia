import java.util.Date;
import java.util.LinkedList;

public class Medico extends Usuario {

    private final LinkedList<AutorizacaoExame> autorizacoesExames;

    public Medico(int codUsuario, String nome, String iniciais) {
        super(codUsuario, nome, iniciais);
        this.autorizacoesExames = new LinkedList<>();
    }

    public void incluirAutorizacaoDeExame(AutorizacaoExame autorizacao) {
        this.autorizacoesExames.add(autorizacao);
    }

    public LinkedList<AutorizacaoExame> exibirAutorizacoesExames() {
        return autorizacoesExames;
//        for (AutorizacaoExame autorizacaoExame : autorizacoesExames) {
//            System.out.print(autorizacaoExame + "\n\n");
//        }
    }

    @Override
    public String toString() {
        return "Medico{" + getCodUsuario() + ", " + getNome() + ", " + getIniciais() + '}';
    }
}