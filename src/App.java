import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class App {

    private static final ArrayList<Medico> medicos = new ArrayList<>();
    private static final ArrayList<Paciente> pacientes = new ArrayList<>();
    private static final ArrayList<Admin> admins = new ArrayList<>();

//construtor

    private static final Scanner input = new Scanner(System.in);

    public static LinkedList<AutorizacaoExame> lista = new LinkedList<AutorizacaoExame>();
    public static LinkedList<AutorizacaoCirurgia> listaCir = new LinkedList<AutorizacaoCirurgia>();

    public static void main(String[] args) {

        try {
            Scanner input = new Scanner(System.in);
            boolean continuaMain = true;
            criaRegistros();
            while (continuaMain) {
                System.out.println("Bem-vindo!");
                System.out.println("[1] Paciente");
                System.out.println("[2] Médico");
                System.out.println("[3] Administrador");
                System.out.println("[4] Sair");
                System.out.print("Digite o número do tipo de usuário desejado:");

                int usuarioSelecionado = input.nextInt();
                switch (usuarioSelecionado) {
                    case 1:
                        //Paciente
                        System.out.println("Código de login: ");
                        int codigoP = input.nextInt();
                        for (int i = 0; i < pacientes.size(); i++) {
                            if (codigoP == pacientes.get(i).getCodUsuario())
                                usuarioPaciente(pacientes.get(i));
                        }
                        break;
                    case 2:
                        //Medico
                        System.out.println("Código de login: ");
                        int codigoM = input.nextInt();
                        for (int i = 0; i < medicos.size(); i++) {
                            if (codigoM == medicos.get(i).getCodUsuario())
                                usuarioMedico(medicos.get(i));
                        }
                        break;
                    case 3:
                        //Administrador
                        System.out.println("Código de login: ");
                        int codigoAdm = input.nextInt();
                        for (int i = 0; i < admins.size(); i++) {
                            if (codigoAdm == admins.get(i).getCodUsuario())
                                usuarioAdm(admins.get(i));
                        }
                        break;
                    case 4:
                        continuaMain = false;
                        System.out.println("Programa Finalizado!");
                        break;
                    default:
                        System.out.println("Opção inválida!");
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Ocorreu uma falha inesperada!");
            System.out.println("Aplicação reinicializada!");
        }
    }

    public static void usuarioPaciente(Paciente paciente) {
        //Funcionalidades do paciente
        Scanner in = new Scanner(System.in);
        boolean continuaPac = true;
        while (continuaPac) {
            System.out.println("\t-- Menu do Paciente " + paciente.getNome() + "-" +paciente.getIniciais()+ " --");
            System.out.println("[1]Marcar exame como realizado.");
            System.out.println("[2]Listar todas as autorizações de exame");
            System.out.println("[3]Voltar ao menu principal.");
            System.out.print("Digite o número da funcionalidade desejada:");
            int op = in.nextInt();
            switch (op) {
                case 1:
                    System.out.println("Autorizações de Exames do Paciente:");
                    for (int i = 0; i < paciente.autorizacoesExames.size(); i++) {
                        System.out.println("["+ i +"] " + paciente.autorizacoesExames.get(i));
                    }
                    int escolhaAuto = 0;
                    AutorizacaoExame autoEscolhido = null;

                    do {
                        System.out.println("Escolha um número: ");
                        escolhaAuto = in.nextInt();
                        if (escolhaAuto >= 0 && escolhaAuto < paciente.autorizacoesExames.size() + 1) {
                            autoEscolhido = paciente.autorizacoesExames.get(escolhaAuto);
                        }
                    }while(escolhaAuto < 0 && escolhaAuto >= paciente.autorizacoesExames.size() + 1);
                    Exame exameEscolhido = autoEscolhido.getExameSoli();
                    System.out.println("Digite o dia da autorização(dd): ");
                    int dia = in.nextInt();
                    System.out.println("Digite o mes da autorização(mm): ");
                    int mes = in.nextInt();
                    System.out.println("Digite o ano da autorização(yyyy): ");
                    int ano = in.nextInt();
                    Date dataRealizado = new Date(ano,mes,dia);
                    exameEscolhido.setRelizado(autoEscolhido.getDataCadastro(),dataRealizado);
                    break;
                case 2:
                    paciente.listarAutorizacoesExames();
                    break;
                case 3:
                    continuaPac = false;
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        }
    }

    public static void usuarioMedico(Medico medico) {
        //Funcionalidades do medico
        Scanner in = new Scanner(System.in);
        boolean continuaMed = true;
        while (continuaMed) {
            System.out.println("\t-- Menu do Médico " + medico.getNome() + "-" +medico.getIniciais()+ " --");
            System.out.println("[1]Incluir nova autorização de exame.");
            System.out.println("[2]Listar todas as autorizações de exame.");
            System.out.println("[3]Incluir nova autorização de cirurgia.");
            System.out.println("[4]Listar todas as autorizações de cirurgia.");
            System.out.println("[5]Voltar ao menu principal.");
            System.out.print("Digite o número da funcionalidade desejada:");
            int op = in.nextInt();
            switch (op) {
                case 1:
                    System.out.println("Digite o dia da autorização(dd): ");
                    int dia = in.nextInt();
                    System.out.println("Digite o mes da autorização(mm): ");
                    int mes = in.nextInt();
                    System.out.println("Digite o ano da autorização(yyyy): ");
                    int ano = in.nextInt();
                    Date dataCadastro = new Date(ano,mes,dia);
                    System.out.println("Pacientes:");
                    for (int i = 0; i < pacientes.size(); i++) {
                        System.out.println("["+ i +"] " + pacientes.get(i).getNome());
                    }
                    int numP = 0;
                    Paciente autoP = null;
                    do {
                        System.out.println("Escolha um número: ");
                        numP = in.nextInt();
                        if (numP >= 0 && numP < pacientes.size() + 1) {
                            autoP = pacientes.get(numP);
                        }
                    }while(numP < 0 && numP >= pacientes.size() + 1);
                    System.out.println("[1] Tomografia\n" +
                            "[2] RaioX\n" +
                            "[3] Hemograma\n" +
                            "[4] RM\n" +
                            "[5] Endoscopia\n" +
                            "[6] Biópsia\n" +
                            "[7] Ultrassom\n" +
                            "[8] Colesterol\n" +
                            "[9] Glicemia\n" +
                            "[10] Angiografia");
                    System.out.println("Digite o número do tipo de exame:");
                    int tipoExame = in.nextInt();
                    Exame exameAuto;
                    if(tipoExame == 1){
                        exameAuto = new Exame(TipoExames.Tomografia);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 2){
                        exameAuto = new Exame(TipoExames.RaioX);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 3) {
                        exameAuto = new Exame(TipoExames.Hemograma);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 4) {
                        exameAuto = new Exame(TipoExames.RM);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 5) {
                        exameAuto = new Exame(TipoExames.Endoscopia);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 6) {
                        exameAuto = new Exame(TipoExames.Biópsia);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 7) {
                        exameAuto = new Exame(TipoExames.Ultrassom);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 8) {
                        exameAuto = new Exame(TipoExames.Colesterol);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 9) {
                        exameAuto = new Exame(TipoExames.Glicemia);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }
                    else if(tipoExame == 10) {
                        exameAuto = new Exame(TipoExames.Angiografia);
                        AutorizacaoExame exame = new AutorizacaoExame(dataCadastro, medico, autoP,exameAuto);
                        medico.incluirAutorizacaoDeExame(exame);
                        autoP.addExame(exame);
                        lista.add(exame);
                    }

                    break;
                case 2:
                    System.out.println("Digite [1] para filtrar por paciente, ou [2] para filtrar por tipo de exame:");
                    int filtro = in.nextInt();
                    if(filtro == 1){
                        System.out.println("Pacientes:");
                        for (int i = 0; i < pacientes.size(); i++) {
                            System.out.println("["+ i +"] " + pacientes.get(i).getNome());
                        }
                        int numFiltroP = 0;
                        Paciente filtroP = null;
                        do {
                            System.out.println("Escolha um número: ");
                            numFiltroP = in.nextInt();
                            if (numFiltroP >= 0 && numFiltroP < pacientes.size() + 1) {
                                filtroP = pacientes.get(numFiltroP);
                            }
                        }while(numFiltroP < 0 && numFiltroP >= pacientes.size() + 1);
                        LinkedList<AutorizacaoExame> list = getAutorizacaoExamebyPaciente(medico,filtroP);
                        for (int i = 0; i < list.size(); i++) {
                            System.out.println(list.get(i));
                        }
                    }
                    else if(filtro == 2){
                        System.out.println("[1] Tomografia\n" +
                                "[2] RaioX\n" +
                                "[3] Hemograma\n" +
                                "[4] RM\n" +
                                "[5] Endoscopia\n" +
                                "[6] Biópsia\n" +
                                "[7] Ultrassom\n" +
                                "[8] Colesterol\n" +
                                "[9] Glicemia\n" +
                                "[10] Angiografia");
                        System.out.println("Digite o número do tipo de exame:");
                        int filtroExame = in.nextInt();
                        TipoExames tipoDoExame = null;
                        switch (filtroExame){
                            case 1:
                                tipoDoExame = TipoExames.Tomografia;
                                break;
                            case 2:
                                tipoDoExame = TipoExames.RaioX;
                                break;
                            case 3:
                                tipoDoExame = TipoExames.Hemograma;
                                break;
                            case 4:
                                tipoDoExame = TipoExames.RM;
                                break;
                            case 5:
                                tipoDoExame = TipoExames.Endoscopia;
                                break;
                            case 6:
                                tipoDoExame = TipoExames.Biópsia;
                                break;
                            case 7:
                                tipoDoExame = TipoExames.Ultrassom;
                                break;
                            case 8:
                                tipoDoExame = TipoExames.Colesterol;
                                break;
                            case 9:
                                tipoDoExame = TipoExames.Glicemia;
                                break;
                            case 10:
                                tipoDoExame = TipoExames.Angiografia;
                                break;
                        }
                        LinkedList<AutorizacaoExame> listTipo = getAutorizacaoExamebyExame(medico, tipoDoExame);
                        for (int i = 0; i < listTipo.size(); i++) {
                            System.out.println(listTipo.get(i) + "\n\n");
                        }

                    }
                    break;
                case 3:
                    System.out.println("Digite o dia da autorização(dd): ");
                    int dia1 = in.nextInt();
                    System.out.println("Digite o mes da autorização(mm): ");
                    int mes1 = in.nextInt();
                    System.out.println("Digite o ano da autorização(yyyy): ");
                    int ano1 = in.nextInt();
                    Date dataCadastro1 = new Date(ano1,mes1,dia1);
                    System.out.println("Pacientes:");
                    for (int i = 0; i < pacientes.size(); i++) {
                        System.out.println("["+ i +"] " + pacientes.get(i).getNome());
                    }
                    int numP1 = 0;
                    Paciente autoP1 = null;
                    do {
                        System.out.println("Escolha um número: ");
                        numP1 = in.nextInt();
                        if (numP1 >= 0 && numP1 < pacientes.size() + 1) {
                            autoP1 = pacientes.get(numP1);
                        }
                    }while(numP1 < 0 && numP1 >= pacientes.size() + 1);
                    System.out.println("[1] Emergência\n" +
                            "[2] Paliativa\n" +
                            "[3] Reconstrutora");
                    System.out.println("Digite o número do tipo de exame:");
                    int tipoC = in.nextInt();
                    Cirurgia c;
                    switch (tipoC){
                        case 1:
                            listaCir.add(new AutorizacaoCirurgia(dataCadastro1,medico,autoP1,Cirurgia.Emergência));
                            break;
                        case 2:
                            listaCir.add(new AutorizacaoCirurgia(dataCadastro1,medico,autoP1,Cirurgia.Paliativa));
                            break;
                        case 3:
                            listaCir.add(new AutorizacaoCirurgia(dataCadastro1,medico,autoP1,Cirurgia.Reconstrutora));
                            break;
                        default:
                            listaCir.add(new AutorizacaoCirurgia(dataCadastro1,medico,autoP1,Cirurgia.Emergência));
                            break;
                    }
                    break;
                case 4:
                    System.out.println("Lista de cirurgias: ");
                    for (int i = 0; i < listaCir.size(); i++) {
                        if(listaCir.get(i).getMedicoSoli()==medico){
                            System.out.println(listaCir.get(i));
                        }
                    }
                    break;
                case 5:
                    continuaMed = false;
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        }
    }

    public static void usuarioAdm(Admin administrador){
        //Funcionalidades do administrador
        Scanner in = new Scanner(System.in);
        boolean continuaAdm = true;
        while (continuaAdm) {
            System.out.println("\t-- Menu do Administrador "+ administrador.getNome() + "-" +administrador.getIniciais()+ " --");
            System.out.println("[1]Incluir novo usuário.");
            System.out.println("[2]Buscar usuário.");
            System.out.println("[3]Estatísticas do programa");
            System.out.println("[4]Voltar ao menu principal.");
            System.out.print("Digite o número da funcionalidade desejada:");
            int op = in.nextInt();
            switch (op) {
                case 1:
                    criaUsuario();
                    break;
                case 2:
                    System.out.println("Digite [1] para buscar por paciente, ou [2] para buscar por médico:");
                    int getUsuario = in.nextInt();
                    clearBuffer(in);
                    if (getUsuario == 1) {
                        System.out.println("Digite o nome do paciente:");
                        String buscaP = in.nextLine();
                        boolean achouP = false;
                        for (int i = 0; i < pacientes.size(); i++) {
                            if (buscaP.equalsIgnoreCase(pacientes.get(i).getNome())) {
                                buscarPaciente(pacientes.get(i).getNome());
                                achouP = true;
                            }
                        }
                        if (!achouP) System.out.println("Paciente não encotrado.");
                    } else if (getUsuario == 2) {
                        System.out.println("Digite o nome do médico:");
                        String buscaM = in.nextLine();
                        boolean achouM = false;
                        for (int i = 0; i < medicos.size(); i++) {
                            if (buscaM.equalsIgnoreCase(medicos.get(i).getNome())) {
                                buscarMedico(medicos.get(i).getNome());
                                achouM = true;
                            }
                        }
                        if (!achouM) System.out.println("Médico não encotrado.");
                    }
                    //listar as autorizações do usuário pesquisado
                    break;
                case 3:
                    verEstatisticas();
                    break;
                case 4:
                    continuaAdm = false;
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        }
    }

    public static void criaUsuario() {
        int ward1 = 0;
        while (ward1 == 0) {
            System.out.println("");
            System.out.println("Qual dos seguintes usuários deseja incluir no sistema:");
            System.out.println("Médico, digite 1.");
            System.out.println("Paciente, digite 2.");
            System.out.println("Administrador, digite 3.");
            String ward2 = input.nextLine();
            if (ward2.matches("[0-9]*")) {
                switch (ward2) {
                    case "1":
                        int teste = 0;
                        int codigoMedico = (int) System.currentTimeMillis();
                        clearBuffer(input);
                        for (int i = 0; i < medicos.size(); i++) {
                            Medico aux = medicos.get(i);
                            int codigoM = aux.getCodUsuario();
                            if (codigoM == codigoMedico) {
                                teste++;
                                System.out.println("O usuário já existe no sistema.");
                                break;
                            }
                        }
                        if (teste == 1) {
                            break;
                        }
                        System.out.println("Digite o nome do médico: ");
                        String nomeMedico = input.nextLine();
                        System.out.println("Digite as iniciais do médico: ");
                        String iniciaisMedico = input.nextLine();
                        Medico medico = new Medico(codigoMedico, nomeMedico, iniciaisMedico);
                        adicionaMedico(medico);
                        System.out.println("Médico adicionado!");
                        break;

                    case "2":

                        int teste2 = 0;
                        int codigoPaciente = (int) System.currentTimeMillis();
                        ;
                        clearBuffer(input);
                        for (int i = 0; i < pacientes.size(); i++) {
                            Paciente aux = pacientes.get(i);
                            int codigoP = aux.getCodUsuario();
                            if (codigoP == codigoPaciente) {
                                teste2++;
                                System.out.println("O usuário já existe no sistema.");
                                break;
                            }
                        }
                        if (teste2 == 1) {
                            break;
                        }
                        System.out.println("Digite o nome do paciente: ");
                        String nomePaciente = input.nextLine();
                        System.out.println("Digite as iniciais do médico: ");
                        String iniciaisPaciente = input.nextLine();
                        Paciente paciente = new Paciente(codigoPaciente, nomePaciente, iniciaisPaciente);
                        adicionaPaciente(paciente);
                        System.out.println("Paciente adicionado!");
                        break;

                    case "3":

                        int teste3 = 0;
                        System.out.println("Digite o código do administrador: ");
                        int codigoAdministrador = input.nextInt();
                        clearBuffer(input);
                        for (int i = 0; i < admins.size(); i++) {
                            Admin aux = admins.get(i);
                            int codigoA = aux.getCodUsuario();
                            if (codigoA == codigoAdministrador) {
                                teste3++;
                                System.out.println("O usuário já existe no sistema.");
                                break;
                            }
                        }
                        if (teste3 == 1) {
                            break;
                        }
                        System.out.println("Digite o nome do administrador: ");
                        String nomeAdministrador = input.nextLine();
                        System.out.println("Digite as iniciais do administrador: ");
                        String iniciaisAdmin = input.nextLine();
                        Admin admin = new Admin(codigoAdministrador, nomeAdministrador, iniciaisAdmin);
                        adicionaAdministrador(admin);
                        System.out.println("Adiministrador adicionado!");
                        break;
                    default:
                        System.out.println("Opção inválida, digite novamente");
                        break;
                }
            } else {
                System.out.println("Opção inválida, digite novamente(somente números).");
            }
            break;
        }
    }


    public static void verEstatisticas() {

        int ward = 1;

        while (ward == 1) {
            System.out.println("Menu de opções:");
            System.out.println("Para consultar o número de médicos, digite 1.");
            System.out.println("Para consultar o número de pacientes, digite 2.");
            System.out.println("Para consultar o número de autorizações emitidas, digite 3.");
            System.out.println("Para consultar o percentual de autorizações com exames já realizados, digite 4.");
            System.out.println("Para encerrar a sessão, digite 5.");
            String entrada = input.nextLine();

            switch (entrada) {

                case "":
                    continue;

                case "1":

                    int numMedicos = medicos.size();
                    System.out.println("Número de médicos: " + numMedicos);
                    break;

                case "2":

                    int numPacientes = pacientes.size();
                    System.out.println("Número de pacientes: " + numPacientes);
                    break;

                case "3":
                    System.out.println("Número de autorizações emitidas: " + lista.size());
                    break;

                case "4":
                    int count12=0;
                    for (int i = 0; i < lista.size(); i++) {
                        if(lista.get(i).getExameSoli().isRelizado()){
                            count12++;
                        }
                    }
                    double percent = count12*100/ (double) lista.size();
                    System.out.println("Percentual de exames realizados: " + percent);
                    break;

                case "5":
                    System.out.println("Sessão encerrada.");
                    ward = 2;
                    break;
            }

        }
    }


    public static LinkedList<AutorizacaoExame> getAutorizacaoExamebyPaciente(Medico medico, Paciente paciente) {
        LinkedList<AutorizacaoExame> autorizacaoExamesPacientes = new LinkedList<AutorizacaoExame>();
        for (int i = 0; i < medico.exibirAutorizacoesExames().size(); i++) {
            if (medico.exibirAutorizacoesExames().get(i).getPaciente() == paciente) {
                autorizacaoExamesPacientes.add(medico.exibirAutorizacoesExames().get(i));
            }
        }
        autorizacaoExamesPacientes.sort((ae1, ae2) -> {
            return ae1.getDataCadastro().compareTo(ae2.getDataCadastro());
        });
        return autorizacaoExamesPacientes;
    }

    public static LinkedList<AutorizacaoExame> getAutorizacaoExamebyExame(Medico medico, TipoExames te) {
        LinkedList<AutorizacaoExame> exames = new LinkedList<AutorizacaoExame>();
        for (int i = 0; i < medico.exibirAutorizacoesExames().size(); i++) {
            if (medico.exibirAutorizacoesExames().get(i).getExameSoli().getTipo() == te) {
                exames.add(medico.exibirAutorizacoesExames().get(i));
            }
        }
        exames.sort((ae1, ae2) -> {
            return ae1.getDataCadastro().compareTo(ae2.getDataCadastro());
        });
        return exames;
    }

    private static void clearBuffer(Scanner scanner) {
        if (scanner.hasNextLine()) {
            scanner.nextLine();
        }
    }

    public static void adicionaMedico(Medico medico) {
        medicos.add(medico);
    }

    public static void adicionaPaciente(Paciente paciente) {
        pacientes.add(paciente);
    }

    public static void adicionaAdministrador(Admin admin) {
        admins.add(admin);
    }

    public static void buscarMedico(String nome) {
        for (Medico m : medicos) {
            if (m.getNome().contains(nome)) {
                m.exibirAutorizacoesExames();
                System.out.println(m);
            }
        }
    }

    public static void buscarPaciente(String nome) {
        for (Paciente p : pacientes) {
            if (p.getNome().contains(nome)) {
                p.exibirAutorizacoesExames();
                System.out.println(p);
            }
        }
    }

    public static void criaRegistros(){
        long codUsuarios = 2;
        pacientes.add(new Paciente((int) codUsuarios,"Teresinha Giovanna Caroline Porto","TGCP"));
        pacientes.add(new Paciente((int) codUsuarios+1,"Leandro Vitor Costa","LVC"));
        pacientes.add(new Paciente((int) codUsuarios+2,"Yago Osvaldo Ramos","YOR"));
        pacientes.add(new Paciente((int) codUsuarios+3,"Luna Nicole Marlene Nunes","LNMN"));
        pacientes.add(new Paciente((int) codUsuarios+4,"José Mário Corte Real","JMCR"));
        pacientes.add(new Paciente((int) codUsuarios+5,"Arthur Sérgio Nathan Brito","ASNB"));
        pacientes.add(new Paciente((int) codUsuarios+6,"Gael Nelson Rodrigues","GNR"));
        pacientes.add(new Paciente((int) codUsuarios+7,"Vinicius Fábio Guilherme Campos","VFGC"));
        pacientes.add(new Paciente((int) codUsuarios+8,"Julio Augusto Rezende","JAR"));
        pacientes.add(new Paciente((int) codUsuarios+9,"Ana Agatha Alana Nascimento","ASNB"));
        medicos.add(new Medico((int) codUsuarios+10,"Raimunda Laís Raquel Araújo","RLRA"));
        medicos.add(new Medico((int) codUsuarios+11,"Laís Analu Santos","LAS"));
        medicos.add(new Medico((int) codUsuarios+12,"Luan Eduardo da Costa","LEC"));
        medicos.add(new Medico((int) codUsuarios+13,"Carlos Gustavo da Luz","CGL"));
        medicos.add(new Medico((int) codUsuarios+14,"Nair Allana Rocha","NAR"));
        admins.add(new Admin(1,"Gabriel Carlos Mateus Lima", "GCML"));
        Exame exameAuto = new Exame(TipoExames.RaioX);
        AutorizacaoExame exame = new AutorizacaoExame(new Date(), medicos.get(0), pacientes.get(1),exameAuto);
        medicos.get(0).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(1).addExame(exame);
        exameAuto = new Exame(TipoExames.Endoscopia);
        exame = new AutorizacaoExame(new Date(), medicos.get(1), pacientes.get(2),exameAuto);
        medicos.get(1).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(2).addExame(exame);
        exameAuto = new Exame(TipoExames.RaioX);
        exame = new AutorizacaoExame(new Date(), medicos.get(1), pacientes.get(3),exameAuto);
        medicos.get(1).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(3).addExame(exame);
        exameAuto = new Exame(TipoExames.Hemograma);
        exame = new AutorizacaoExame(new Date(), medicos.get(2), pacientes.get(5),exameAuto);
        medicos.get(2).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(5).addExame(exame);
        exameAuto = new Exame(TipoExames.Hemograma);
        exame = new AutorizacaoExame(new Date(), medicos.get(3), pacientes.get(7),exameAuto);
        medicos.get(3).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(7).addExame(exame);
        exameAuto = new Exame(TipoExames.Hemograma);
        exame = new AutorizacaoExame(new Date(), medicos.get(4), pacientes.get(6),exameAuto);
        medicos.get(4).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(6).addExame(exame);
        exameAuto = new Exame(TipoExames.Tomografia);
        exame = new AutorizacaoExame(new Date(), medicos.get(4), pacientes.get(6),exameAuto);
        medicos.get(4).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(6).addExame(exame);
        exameAuto = new Exame(TipoExames.Tomografia);
        exame = new AutorizacaoExame(new Date(), medicos.get(1), pacientes.get(8),exameAuto);
        exameAuto.setRelizado(exame.getDataCadastro(),new Date());
        medicos.get(1).incluirAutorizacaoDeExame(exame);
        lista.add(exame);
        pacientes.get(8).addExame(exame);
        listaCir.add(new AutorizacaoCirurgia(new Date(),medicos.get(0),pacientes.get(0),Cirurgia.Paliativa));
        listaCir.add(new AutorizacaoCirurgia(new Date(),medicos.get(0),pacientes.get(0),Cirurgia.Reconstrutora));
    }

}
