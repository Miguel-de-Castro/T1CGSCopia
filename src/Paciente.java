import java.util.Date;
import java.util.LinkedList;

public class Paciente extends Usuario{

    public final LinkedList<AutorizacaoExame> autorizacoesExames;

    public Paciente(int codUsuario, String nome, String iniciais){
        super(codUsuario, nome, iniciais);
        this.autorizacoesExames = new LinkedList<>();
    }

    public void listarAutorizacoesExames(){
        this.autorizacoesExames.sort((ae1, ae2) -> {
            return ae1.getDataCadastro().compareTo(ae2.getDataCadastro());
        });
        for(AutorizacaoExame ae : this.autorizacoesExames){
            //Se formos printar os valores no terminal.
            System.out.println("[Autorizacao Exame]\n" + ae + "\n\n");
        }
    }

    public void exibirAutorizacoesExames() {
        for (AutorizacaoExame autorizacaoExame : autorizacoesExames) {
            System.out.print(autorizacaoExame + "\n\n");
        }
    }

    public void addExame(AutorizacaoExame exame){
        autorizacoesExames.add(exame);
    }

    @Override
    public String toString() {
        return "Paciente{" + getCodUsuario() + ", " + getNome() + ", " + getIniciais() + '}';
    }
}